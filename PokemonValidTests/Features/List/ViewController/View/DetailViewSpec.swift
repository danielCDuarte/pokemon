//
//  DetailViewSpec.swift
//  PokemonValidTests
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import PokemonValid

class DetailViewSpec: QuickSpec {
    override func spec() {
        describe("DetailViewSpec") {
            
            var sut: DetailView!
            var mainVC: UIViewController!
            beforeEach {
                
                sut = DetailView()
                let pokemonDetail = PokemonDetail(id: 1, name: "bulbasaur", baseExperience: 64, height: 7, weight: 69, imageurl: "", abilities: [Ability(abilityDesc: AbilityItem(name: "overgrow", url: ""))])
                sut.setupView(pokemonDetail: pokemonDetail)
                mainVC = UIViewController()
                mainVC.view = sut
                mainVC.view.backgroundColor = .white
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("DetailViewSpec")
                })
            })
        }
    }
}
