//
//  ListViewSpec.swift
//  PokemonValidTests
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import PokemonValid

class ListViewSpec: QuickSpec {
    override func spec() {
        describe("ListViewSpec") {
            
            var sut: ListView!
            var mainVC: UIViewController!
            beforeEach {
                var delegate: ListViewDelegate?
                sut = ListView(delegate: delegate)
                let pokemonDetail:[PokemonItem] = [PokemonItem(id: 1, name: "bullbasaur", url: ""), PokemonItem(id: 2, name: "charmander", url: ""), PokemonItem(id: 3, name: "Pikachu", url: "")]
                
                sut.setup(pokemonList: pokemonDetail)
                mainVC = UIViewController()
                mainVC.view = sut
                mainVC.view.backgroundColor = .white
                UIWindow.setTestWindow(rootViewController: mainVC)
                
            }
            afterEach {
                UIWindow.cleanTestWindow()
            }
            
            context("When View controller is instanciated", {
                it("Should be a valid snapshot", closure: {
                    expect(UIWindow.testWindow) == snapshot("ListViewSpec")
                })
            })
        }
    }
}
