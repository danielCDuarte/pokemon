//
//  Color.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let background = UIColor.create(216, 241, 241)
    static let fontTitle = UIColor.create(14, 49, 210)
    static let navTitle = UIColor.create(229, 195, 66)
    static let loading = UIColor.create(203, 60, 82)
    static let black_opacity = UIColor.create(0, 0, 0, 0.3)
}
