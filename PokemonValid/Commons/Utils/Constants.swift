//
//  Constants.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation


struct Server {
    static let baseURL = "https://pokeapi.co/api/v2/pokemon"
    static let baseImg = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    static let backgroundImage = "https://pokeres.bastionbot.org/images/pokemon/"
    
}
