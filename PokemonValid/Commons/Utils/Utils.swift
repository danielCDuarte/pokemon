//
//  Utils.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation

class Utils{
    
    static func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func downloadImage(Url:String,onCompletion:@escaping(Data)->Void){
        if Url != "" {
            let url = URL(string: Url)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    onCompletion(data!)
                }
            }
        }
        
    }
    
}
