//
//  ListViewDelegate.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation

protocol ListViewDelegate: class {
    func selectItem(pokemon:PokemonItem)
}
