//
//  PokemonTableViewCell.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit
import Reusable


class PokemonTableViewCell: UITableViewCell, Reusable{
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 10
        return stackView
    }()
    
    private lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        label.numberOfLines = 0
        return label
    }()
    
    
    // MARK: - init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - View Configuration

extension PokemonTableViewCell: ViewConfiguration {
    func configureViews() {
        backgroundColor = Colors.background
    }
    
    func configure(with pokemon:PokemonItem) {
        self.titleLabel.text = pokemon.name
        self.iconImageView.image = UIImage(named: "esfera")
        
        if let pokemonId = pokemon.id {
            Utils.downloadImage(Url: "\(Server.baseImg)\(String(pokemonId)).png") { (data) in
                self.iconImageView.image = UIImage(data: data)
            }
        }
    }
    
    
    func buildViewHierarchy() {
        contentView.addSubview(stackView)
        contentView.addSubview(bottomLine)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(titleLabel)
        
    }
    
    func setupConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(bottomLine.snp.top)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        iconImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.height.equalTo(60)
            make.width.equalTo(60)
        }
    }
    
}

