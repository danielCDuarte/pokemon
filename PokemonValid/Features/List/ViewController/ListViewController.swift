//
//  ListViewController.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,ListViewProtocol{
    
    // MARK: - Properties
    
    var presenter:ListPresenterProtocol?
    var listPokemon = [PokemonItem]()
    private var pokemonSelect :PokemonItem?
    
    private lazy var listView: ListView = {
        let view = ListView(delegate: self)
        return view
    }()
    
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = ""
        ListWireframe.createRecipeModule(listRef: self)
        self.view = listView
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.showBlurLoader()
        presenter?.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // needed to clear the text in the back navigation:
        self.navigationItem.title = " "
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationItem.title = "Pokémon"
    }
    
    func showListPokemon(with listPokemon: [PokemonItem]) {
        self.listPokemon = listPokemon
        if let title = UserDefaults.standard.string(forKey: "titlePokemon"){
            self.title = title
        }
        self.listView.setup(pokemonList: self.listPokemon)
        self.view.removeBluerLoader()
    }
    
    func showDetailPokemon(with detailPokemon: PokemonDetail) {
        self.presenter?.showDetail(with: detailPokemon, from: self)
    }
    
}


extension ListViewController: ListViewDelegate{
    func selectItem(pokemon: PokemonItem) {
        self.view.showBlurLoader()
        presenter?.openDetailPokemon(with: pokemon.url)
        
    }
    
}
