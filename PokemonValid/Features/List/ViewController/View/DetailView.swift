//
//  DetailView.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit

class DetailView: UIView {
    // MARK: - properties
    
    private(set) lazy var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.backgroundColor = .white
        return view
    }()
    
    private(set) lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var imageView: UIImageView = {
        let photoImageVIew = UIImageView()
        photoImageVIew.contentMode = .scaleAspectFit
        photoImageVIew.image = UIImage(named: "esfera")
        return photoImageVIew
    }()
    
    private(set) lazy var baseExperienceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return label
    }()

    private(set) lazy var heightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return label
    }()
    
    private(set) lazy var weightLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return label
    }()
    
    
    private(set) lazy var abilitiesLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        return label
    }()
    
    private lazy var abilitiesStackView : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
    }()
    
    
    
    // MARK: - init
    init() {
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup View
    private var pokemonDetail: PokemonDetail?
    
    func setupView(pokemonDetail:PokemonDetail?){
        
        if let pokemon = pokemonDetail{
            Utils.downloadImage(Url: pokemon.imageurl) { (data) in
                self.imageView.image = UIImage(data: data)
            }

            baseExperienceLabel.text = "Experiencia base: \(pokemon.baseExperience)"
            heightLabel.text = "Altura: \(String(pokemon.height))"
            weightLabel.text =  "Peso: \(String(pokemon.weight))"
            abilitiesLabel.text = "Habilidades:"
            self.renderAbilitites(abilities: pokemon.abilities)
            
        }
    }
    
    private func renderAbilitites(abilities: [Ability]){
        
        for ability in abilities {
            let abilityCell = AbilityCellView(ability: ability.abilityDesc)
            abilitiesStackView.addArrangedSubview(abilityCell)
            abilityCell.snp.makeConstraints { (make) in
                make.height.equalTo(40)
                make.width.equalToSuperview()
            }
        }
        
    }
}

// MARK: - View Configuration

extension DetailView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = Colors.background
        self.scrollView.backgroundColor = Colors.background
        self.containerView.backgroundColor = Colors.background
    }
    func buildViewHierarchy() {
        addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(imageView)
        containerView.addSubview(baseExperienceLabel)
        containerView.addSubview(heightLabel)
        containerView.addSubview(weightLabel)
        containerView.addSubview(abilitiesLabel)
        containerView.addSubview(abilitiesStackView)
        
        
    }
    
    func setupConstraints() {
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().priority(.low)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(200)
        }
        
        baseExperienceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        heightLabel.snp.makeConstraints { (make) in
            make.top.equalTo(baseExperienceLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        weightLabel.snp.makeConstraints { (make) in
            make.top.equalTo(heightLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        abilitiesLabel.snp.makeConstraints { (make) in
            make.top.equalTo(weightLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        abilitiesStackView.snp.makeConstraints { (make) in
            make.top.equalTo(abilitiesLabel.snp.bottom).offset(16)
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-16)
        }
        
    }
    
}
