//
//  ListView.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit
import Reusable

class ListView: UIView {
    // MARK: - properties
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        
        return tableView
    }()
    
    
    private(set) lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.barTintColor = Colors.background
        return searchBar
    }()

    // MARK: - init
    private weak var delegate: ListViewDelegate?
    
    init(delegate: ListViewDelegate?) {
        self.delegate = delegate
        super.init(frame: .zero)
        setupViewConfiguration()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableHeaderView = self.searchBar
        tableView.backgroundColor = Colors.background
        searchBar.delegate = self
        searchBar.sizeToFit()
        tableView.register(cellType: PokemonTableViewCell.self)
     }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - setup
    var pokemonList = [PokemonItem]()
    var filteredPokemonList = [PokemonItem]()
    
    func setup(pokemonList:[PokemonItem]){
        self.pokemonList = pokemonList
        self.filteredPokemonList = pokemonList
        self.tableView.reloadData()
    }

}

// MARK: - View Configuration
extension ListView: ViewConfiguration {
    func configureViews() {
        self.backgroundColor = .white
        self.tableView.backgroundColor = .white
    }
    func buildViewHierarchy() {
        self.addSubview(tableView)
    }
    
    func setupConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
    }
    
}


extension ListView: UITableViewDelegate, UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 88.0
    }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return filteredPokemonList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let pokemonItem = self.filteredPokemonList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: PokemonTableViewCell.self)
        cell.configure(with: pokemonItem)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let pokemonItem = self.filteredPokemonList[indexPath.row]
        self.delegate?.selectItem(pokemon: pokemonItem)
    }
}

extension ListView: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.filteredPokemonList = self.pokemonList.filter { (pokemon: PokemonItem) -> Bool in
            if let searchText = self.searchBar.text?.lowercased() {
                return pokemon.name.lowercased().contains(searchText)
            } else {
                return false
            }
        }
        
        self.tableView.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            self.filteredPokemonList = pokemonList
            self.tableView.reloadData()
            
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
            
        }
    }
    
    
}
