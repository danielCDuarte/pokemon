//
//  AbilityCellView.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit
import SnapKit

class AbilityCellView: UIView {
    
    private lazy var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 10
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        return stackView
    }()
    
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(name: "AvenirNext-Medium", size: 16)
        label.minimumScaleFactor = 0.2
        return label
    }()
    
    
    // MARK: - init
    private let ability: AbilityItem
    init( ability: AbilityItem) {
        self.ability = ability
        super.init(frame: .zero)
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

// MARK: - configurations
extension AbilityCellView: ViewConfiguration {
    
    func configureViews() {
        nameLabel.text = "- \(self.ability.name)"
    }
    
    func buildViewHierarchy() {
        addSubview(containerStackView)
        containerStackView.addArrangedSubview(nameLabel)
    }
    
    func setupConstraints() {
        containerStackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    
}
