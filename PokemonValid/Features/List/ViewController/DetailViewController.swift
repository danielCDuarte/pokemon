//
//  DetailViewController.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit


class DetailViewController: UIViewController, DetailViewProtocol{
   
    // MARK: - Properties
    
    private lazy var detailView: DetailView = {
        let view = DetailView()
        return view
    }()
    
    // MARK: - init
    var presenter: DetailPresenterProtocol?
    var pokemonDetail: PokemonDetail?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.pokemonDetail?.name
        presenter?.viewDidLoad(detail: self.pokemonDetail)
        self.view = detailView
        
    }

    func showDetail(with detail: PokemonDetail?) {
        self.detailView.setupView(pokemonDetail: detail)
    }

}
