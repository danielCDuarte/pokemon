//
//  Service.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation
import Alamofire

class Service {
    
    static func getListPokemon(onCompletion:@escaping([PokemonItem])->Void,onError:@escaping(Error)->Void){
        
        Alamofire.request("\(Server.baseURL)?limit=151").validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON(completionHandler: {response in
            
            switch response.result{
            case .success:
                
                if let jsonResponse = response.result.value {
                    let json = jsonResponse as? [String: Any]
                    let jsonDataString:String = Utils.json(from: json!["results"] as Any)!
                    if let jsonData = jsonDataString.data(using: .utf8)
                    {
                        do{
                            let response = try JSONDecoder().decode([PokemonItem].self, from: jsonData)
                            onCompletion(response)
                            
                        }catch{
                            onError(error)
                        }
                    }
                }
                break
            case .failure(let error):
                onError(error)
            }
        })
    }
    
    static func getDetailPokemon(url:String,onCompletion:@escaping(PokemonDetail)->Void,onError:@escaping(Error)->Void){
        
        Alamofire.request(url).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON(completionHandler: {response in
            switch response.result{
            case .success:
                
                if let jsonResponse = response.result.value {
                    let json = jsonResponse as? [String: Any]
                    let jsonDataString:String = Utils.json(from: json as Any)!
                    if let jsonData = jsonDataString.data(using: .utf8)
                    {
                        do{
                            let response = try JSONDecoder().decode(PokemonDetail.self, from: jsonData)
                            onCompletion(response)
                            
                        }catch{
                            onError(error)
                        }
                    }
                }
                break
            case .failure(let error):
                onError(error)
            }
            
        })
        
    }
    
}
