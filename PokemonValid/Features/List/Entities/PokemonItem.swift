//
//  PokemonItem.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation

class PokemonItem: Codable {
    let name: String
    let url: String
    let id: Int?
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
   
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        name = try values.decode(String.self, forKey: .name)
        url = try values.decode(String.self, forKey: .url)
        let getId:String = url.replacingOccurrences(of: Server.baseURL, with: "")
        let clearGetId:String = getId.replacingOccurrences(of: "/", with: "")
        id = Int(clearGetId)
        
    }
    
    
    init(id: Int,name :String, url:String) {
        self.id = id
        self.name = name
        self.url = url
    }
    
}
