//
//  Ability.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation


class Ability: Codable {
    let abilityDesc: AbilityItem
    
    enum CodingKeys: String, CodingKey {
        case abilityDesc = "ability"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        abilityDesc = try values.decode(AbilityItem.self, forKey: .abilityDesc)
        
    }
    
    init(abilityDesc :AbilityItem) {
        self.abilityDesc = abilityDesc
    }
}
