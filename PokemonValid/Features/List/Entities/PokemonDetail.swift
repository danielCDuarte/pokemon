//
//  PokemonDetail.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation


class PokemonDetail: Codable {
    let id : Int
    let name: String
    let baseExperience:Int
    let height: Int
    let weight: Int
    let imageurl: String
    let abilities:[Ability]
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case baseExperience = "base_experience"
        case height = "height"
        case weight = "weight"
        case abilities = "abilities"
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name).uppercased()
        baseExperience = try values.decode(Int.self, forKey: .baseExperience)
        height = try values.decode(Int.self, forKey: .height)
        weight = try values.decode(Int.self, forKey: .weight)
        imageurl = "\(Server.backgroundImage)\(String(id)).png"
        
        let abilities = try values.decodeIfPresent([Ability].self, forKey: .abilities)
        self.abilities = abilities ?? []
        
    }
    
    
    init(id: Int,name :String, baseExperience:Int, height:Int, weight:Int, imageurl:String, abilities:[Ability]) {
        self.id = id
        self.name = name
        self.baseExperience = baseExperience
        self.height =  height
        self.weight = weight
        self.imageurl = imageurl
        self.abilities =  abilities
    }
    
}

