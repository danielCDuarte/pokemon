//
//  ListInteractor.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import Foundation
import Network


class ListInteractor: ListInputInteractorProtocol {
  
    weak var presenter: ListOutputInteractorProtocol?
    
    
    func getListPokemon() {
        Service.getListPokemon(onCompletion: { (listPokemon) in
            self.presenter?.pokemonListDidFetch(list: listPokemon)
        }) { (error) in
            self.presenter?.didErrorFetch(error: error)
        }
    }
    
    
    func getItemPokemon(url: String) {
        Service.getDetailPokemon(url: url, onCompletion: { (PokemonDetail) in
            self.presenter?.pokemonDidFetch(pokemonDetail: PokemonDetail)
        }) { (error) in
            self.presenter?.didErrorFetch(error: error)
        }
    }
    
    
}
