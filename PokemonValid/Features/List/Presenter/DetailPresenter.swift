//
//  DetailPresenter.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class DetailPresenter: DetailPresenterProtocol {

    var interactor: DetailInputInteractorProtocol?
    var view: DetailViewProtocol?
    var wireframe: DetailWireFrameProtocol?
    var presenter:DetailPresenterProtocol?
    
    func viewDidLoad(detail: PokemonDetail?) {
        view?.showDetail(with: detail)
    }
    
    func Detail(detail: PokemonDetail) {
        interactor?.Detail(detail: detail)
    }
 
}

extension DetailPresenter: DetailOutputInteractorProtocol{

}
