//
//  ListPresenter.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class ListPresenter: ListPresenterProtocol {

   
    var interactor: ListInputInteractorProtocol?
    var view: ListViewProtocol?
    var wireframe: ListWireFrameProtocol?
    var presenter: ListPresenterProtocol?
    
    func viewDidLoad() {
        self.loadList()
    }
    
    func loadList() {
        interactor?.getListPokemon()
    }
    
    func openDetailPokemon(with url: String) {
        interactor?.getItemPokemon(url: url)
    }
    
    func showDetail(with pokemonDetail: PokemonDetail?, from view: UIViewController) {
        wireframe?.pushToManagePokemon(with: pokemonDetail, from: view)
    }
    
}

extension ListPresenter:ListOutputInteractorProtocol {
    
    func pokemonDidFetch(pokemonDetail: PokemonDetail) {
        view?.showDetailPokemon(with: pokemonDetail)
    }
    
  
    func pokemonListDidFetch(list: [PokemonItem]) {
        view?.showListPokemon(with: list)
    }
    
    func pokemonDidFetch(pokemonItem: PokemonDetail, from view: UIViewController) {
        wireframe?.pushToManagePokemon(with: pokemonItem, from: view)
    }
    
    
    func didErrorFetch(error: Error) {
        print("error",error)
    }
    
}
