//
//  DetailProtocols.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

protocol DetailViewProtocol: class {
    //Presenter -> View
    func showDetail(with detail: PokemonDetail?)

}

protocol DetailPresenterProtocol: class {
    //View -> Presenter
    
    var interactor: DetailInputInteractorProtocol? {get set}
    var wireframe: DetailWireFrameProtocol? {get set}
    var view: DetailViewProtocol? {get set}
    
    func viewDidLoad(detail:PokemonDetail?)
    func Detail(detail:PokemonDetail)
    
}

protocol DetailInputInteractorProtocol: class {
    var presenter: DetailOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func Detail(detail:PokemonDetail)
}

protocol DetailOutputInteractorProtocol: class {
    //Interactor -> Presenter
}

protocol DetailWireFrameProtocol: class {
    func goBackToListView(from view: UIViewController)
    static func createDetailModule(DetailRef: DetailViewController,detail: PokemonDetail?)
}
