//
//  ListProtocols.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

protocol ListViewProtocol: class {
    // PRESENTER -> VIEW
    func showListPokemon(with listPokemon: [PokemonItem])
    func showDetailPokemon(with detailPokemon:PokemonDetail)
}

protocol ListPresenterProtocol: class {
    //View -> Presenter
    var interactor: ListInputInteractorProtocol? {get set}
    var view: ListViewProtocol? {get set}
    var wireframe: ListWireFrameProtocol? {get set}
    
    func viewDidLoad()
    func openDetailPokemon(with url: String)
    func showDetail(with pokemonDetail: PokemonDetail?, from view: UIViewController)
}

protocol ListInputInteractorProtocol: class {
    var presenter: ListOutputInteractorProtocol? {get set}
    //Presenter -> Interactor
    func getListPokemon()
    func getItemPokemon(url:String)
}

protocol ListOutputInteractorProtocol: class {
    //Interactor -> Presenter
    func pokemonListDidFetch(list: [PokemonItem])
    func didErrorFetch(error:Error)
    
    func pokemonDidFetch(pokemonDetail:PokemonDetail)
}

protocol ListWireFrameProtocol: class {
    //Presenter -> Wireframe
    func pushToManagePokemon(with pokemonDetail: PokemonDetail?, from view: UIViewController)
    static func createRecipeModule(listRef: ListViewController)
}
