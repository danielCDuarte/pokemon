//
//  ListWireframe.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class ListWireframe: ListWireFrameProtocol {
    
    
    static func createRecipeModule(listRef: ListViewController) {
        let presenter: ListPresenterProtocol & ListOutputInteractorProtocol = ListPresenter()
        listRef.presenter = presenter
        listRef.presenter?.wireframe = ListWireframe()
        listRef.presenter?.view = listRef
        listRef.presenter?.interactor = ListInteractor()
        listRef.presenter?.interactor?.presenter = presenter
    }
    
    func pushToManagePokemon(with pokemonDetail: PokemonDetail?, from view: UIViewController) {
        let detailVC = DetailViewController()
        DetailWireframe.createDetailModule(DetailRef: detailVC, detail: pokemonDetail)
        view.navigationController?.pushViewController(detailVC, animated: true)
    }

}
