//
//  DetailWireframe.swift
//  PokemonValid
//
//  Created by Dani Riders on 10/05/20.
//  Copyright © 2020 daniel-crespo. All rights reserved.
//

import UIKit

class DetailWireframe: DetailWireFrameProtocol {
  
    static func createDetailModule(DetailRef: DetailViewController, detail: PokemonDetail?) {
        let presenter: DetailPresenterProtocol & DetailOutputInteractorProtocol = DetailPresenter()
        DetailRef.presenter = presenter
        DetailRef.pokemonDetail = detail
        DetailRef.presenter?.wireframe = DetailWireframe()
        DetailRef.presenter?.view = DetailRef
        DetailRef.presenter?.interactor = DetailInteractor()
        DetailRef.presenter?.interactor?.presenter = presenter
    }
    
    func goBackToListView(from view: UIViewController) {
        
    }
    
}
